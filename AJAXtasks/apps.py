from django.apps import AppConfig


class AjaxtasksConfig(AppConfig):
    name = 'AJAXtasks'
