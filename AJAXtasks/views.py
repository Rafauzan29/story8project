from django.shortcuts import render, redirect
from django.http import JsonResponse
import requests
import json

# Create your views here.

def landingPage (request):
    return render(request, 'landingPage.html')

def searchView(request, key):
    result = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + key).json()
    return JsonResponse(result)

