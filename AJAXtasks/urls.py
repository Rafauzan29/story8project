from django.urls import path
from . import views

urlpatterns = [
	path('', views.landingPage, name = 'landingPage'),
	path('searchResult/<str:key>', views.searchView, name="search")
]