from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import *
from django.apps import apps
from .apps import AjaxtasksConfig

# Create your tests here.

class AppTest(TestCase):

	def test_AjaxtasksApps(self):
		self.assertEqual(AjaxtasksConfig.name, 'AJAXtasks') 
		self.assertEqual(apps.get_app_config('AJAXtasks').name, 'AJAXtasks')



class viewsTesting (TestCase):
    def test1_viewsTestJson(self):
        response = Client().get('/searchResult/test')
        self.assertContains(response, "https://www.googleapis.com")
